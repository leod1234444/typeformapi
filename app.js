const express = require('express');
const fs = require('fs');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const cors = require('cors');  // Importer le middleware CORS

const app = express();
app.use(express.json());
app.use(cors());  // Utiliser le middleware CORS

// Clé secrète pour signer les tokens JWT
const JWT_SECRET = 'your_jwt_secret_key';  // Remplacez par une clé sécurisée

// Chemin vers le fichier JSON de la base de données
const dbFilePath = './database.json';

// Fonction pour lire la base de données depuis le fichier JSON
function readDatabase() {
    if (fs.existsSync(dbFilePath)) {
        const data = fs.readFileSync(dbFilePath, 'utf-8');
        return JSON.parse(data);
    } else {
        return { users: [] };
    }
}

// Fonction pour écrire la base de données dans le fichier JSON
function writeDatabase(database) {
    fs.writeFileSync(dbFilePath, JSON.stringify(database, null, 2));
}

// Fonction pour générer des IDs uniques (simple pour cet exemple)
function generateId(data) {
    if (data.length === 0) {
        return "1";
    } else {
        return (parseInt(data[data.length - 1].user_id) + 1).toString();
    }
}

// Fonction pour générer des IDs uniques pour les formulaires (simple pour cet exemple)
function generateFormId() {
    const database = readDatabase();
    let maxFormId = 0;

  // Parcourir tous les utilisateurs et leurs formulaires pour trouver le plus grand form_id
  database.users.forEach(user => {
    user.forms.forEach(form => {
      const formId = parseInt(form.form_id, 10);
      if (formId > maxFormId) {
        maxFormId = formId;
      }
    });
  });

  // Retourner un nouvel form_id incrémenté
  return (maxFormId + 1).toString();
}

// Fonction pour hasher un mot de passe en SHA1
function hashPassword(password) {
    return crypto.createHash('sha1').update(password).digest('hex');
}

// Middleware pour vérifier le token JWT
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (token == null) return res.sendStatus(401);

    jwt.verify(token, JWT_SECRET, (err, user) => {
        if (err) return res.sendStatus(403);
        req.user = user;
        next();
    });
}

// Endpoint pour créer un utilisateur
app.post('/users', (req, res) => {
    const data = req.body;
    if (!data || !data.username || !data.email || !data.password) {
        return res.status(400).json({ error: "Invalid data" });
    }

    const database = readDatabase();

    // Vérifier si l'email existe déjà
    const existingUser = database.users.find(user => user.email === data.email);
    if (existingUser) {
        return res.status(400).json({ error: "Email already exists" });
    }

    const newUser = {
        user_id: generateId(database.users),
        username: data.username,
        email: data.email,
        password: hashPassword(data.password), // Hash du mot de passe
        forms: []
    };

    database.users.push(newUser);
    writeDatabase(database);
    delete newUser.password;  // Ne pas renvoyer le mot de passe
    res.status(201).json(newUser);
});

// Endpoint pour se connecter et obtenir un token JWT
app.post('/login', (req, res) => {
    const { email, password } = req.body;
    const database = readDatabase();

    const user = database.users.find(u => u.email === email && u.password === hashPassword(password));
    if (!user) {
        return res.status(400).json({ error: "Invalid email or password" });
    }

    const accessToken = jwt.sign({ user_id: user.user_id, username: user.username }, JWT_SECRET, { expiresIn: '1h' });
    res.json({ accessToken });
});

// Endpoint protégé pour créer un formulaire
app.post('/forms', authenticateToken, (req, res) => {
    const data = req.body;
    if (!data || !data.form_title || !Array.isArray(data.questions)) {
        return res.status(400).json({ error: "Invalid data" });
    }

    const database = readDatabase();
    const user = database.users.find(u => u.user_id === req.user.user_id);

    if (!user) {
        return res.status(404).json({ error: "User not found" });
    }

    const newForm = {
        form_id: generateFormId(user.forms),
        form_title: data.form_title,
        questions: data.questions.map((q, index) => ({
            question_id: (index + 1).toString(),
            question_type: q.question_type,
            question_text: q.question_text,
            options: q.options || [],
            correct_answer: q.correct_answer || null,
            correct_answers: q.correct_answers || []
        })),
        responses: []
    };

    user.forms.push(newForm);
    writeDatabase(database);
    res.status(201).json(newForm);
});

// Endpoint pour récupérer un formulaire par ID
app.get('/forms/:formId', (req, res) => {
    const formId = req.params.formId;
    const database = readDatabase();
    
    for (const user of database.users) {
        const form = user.forms.find(f => f.form_id === formId);
        if (form) {
            return res.status(200).json(form);
        }
    }

    res.status(404).json({ error: "Form not found" });
});

// Endpoint pour répondre à un formulaire par ID
app.post('/forms/:formId/responses', (req, res) => {
    const formId = req.params.formId;
    const data = req.body;
    if (!data || !Array.isArray(data.responses)) {
        return res.status(400).json({ error: "Invalid data" });
    }

    const database = readDatabase();
    for (const user of database.users) {
        const form = user.forms.find(f => f.form_id === formId);
        if (form) {
            form.responses.push({
                response_id: form.responses.length + 1,
                answers: data.responses
            });
            writeDatabase(database);
            return res.status(201).json({ message: "Response recorded" });
        }
    }

    res.status(404).json({ error: "Form not found" });
});

// Endpoint protégé pour récupérer les réponses d'un formulaire
app.get('/forms/:formId/responses', authenticateToken, (req, res) => {
    const formId = req.params.formId;
    const database = readDatabase();
    const user = database.users.find(u => u.user_id === req.user.user_id);

    if (!user) {
        return res.status(404).json({ error: "User not found" });
    }

    const form = user.forms.find(f => f.form_id === formId);
    if (!form) {
        return res.status(404).json({ error: "Form not found" });
    }

    res.status(200).json(form.responses);
});

// Endpoint protégé pour récupérer tous les IDs des formulaires de l'utilisateur
app.get('/forms', authenticateToken, (req, res) => {
    const database = readDatabase();
    const user = database.users.find(u => u.user_id === req.user.user_id);

    if (!user) {
        return res.status(404).json({ error: "User not found" });
    }

    const formIds = user.forms.map(form => form.form_id);
    res.status(200).json(formIds);
});

// Endpoint protégé pour exemple (requiert authentification)
app.get('/protected', authenticateToken, (req, res) => {
    res.json({ message: "This is a protected route", user: req.user });
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
